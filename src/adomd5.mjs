// A modified MD5 hasher that produces non-standard hashes.

import buffer from 'buffer';

import MD5 from 'md5.js';

const {Buffer} = buffer;

export class AdoMd5 extends MD5 {
	constructor() {
		super();

		// Patch buffer reading and writing functions to use big endian.
		const block = this._block;
		block.readInt32LE = block.readInt32BE;
		block.readUInt32LE = block.readUInt32BE;
		block.writeInt32LE = block.writeInt32BE;
		block.writeUInt32LE = block.writeUInt32BE;
	}

	_digest() {
		// Finish hash, then recreate result in big endian.
		super._digest();
		const buffer = Buffer.allocUnsafe(16);
		buffer.writeInt32BE(this._a, 0);
		buffer.writeInt32BE(this._b, 4);
		buffer.writeInt32BE(this._c, 8);
		buffer.writeInt32BE(this._d, 12);
		return buffer;
	}
}
