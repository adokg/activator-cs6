import buffer from 'buffer';

const {Buffer} = buffer;

export function platformClean(serial) {
	return serial.replace(/[^a-z0-9]/ig, '');
}

export function serialClean(serial) {
	return serial.replace(/[^0-9]/g, '');
}

export function codeClean(serial) {
	return serial.replace(/[^a-z0-9+\/=]/ig, '');
}

export function codeFormat(serial, group) {
	const chars = serial.split('').reverse();
	let r = '';
	for (let i = 0; chars.length; i++) {
		if (i && (i % group) === 0) {
			r += ' ';
		}
		r += chars.pop();
	}
	return r;
}

export function bufferFrom(data, encoding = null) {
	// Use modern from method if available.
	if (Buffer.from) {
		return encoding ? Buffer.from(data, encoding) : Buffer.from(data);
	}
	return encoding ? new Buffer(data, encoding) : new Buffer(data);
}

export function base64Decode(str) {
	return new Uint8Array(bufferFrom(str, 'base64'));
}

export function base64Encode(data) {
	return bufferFrom(data).toString('base64');
}

export function uint8ArrayToHex(a, delim = ' ') {
	const r = [];
	for (let i = 0; i < a.length; i++) {
		const v = a[i];
		r.push((v < 0x10 ? '0' : '') + v.toString(16).toUpperCase());
	}
	return r.join(delim);
}

export function uint8ArrayFromHex(str) {
	const matches = str.replace(/\s/g, '').match(/../g);
	const a = matches ? [...matches] : [];
	const r = new Uint8Array(a.length);
	for (let i = 0; i < a.length; i++) {
		r[i] = parseInt(a[i], 16);
	}
	return r;
}

export function uint8ArraySlice(a, offset = 0, size = null) {
	size = size === null ? a.length - offset : size;
	const r = new Uint8Array(size);
	for (let i = 0; i < size; i++) {
		r[i] = a[i + offset];
	}
	return r;
}
