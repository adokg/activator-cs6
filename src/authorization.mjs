import {
	checksumTable,
	aesIV
} from './data';
import {
	platformClean,
	serialClean,
	codeClean,
	codeFormat,
	bufferFrom,
	base64Decode,
	base64Encode,
	uint8ArrayFromHex,
	uint8ArraySlice
} from './util';
import {Aes128Cbc} from './aes128cbc';
import {AdoMd5} from './adomd5';

export class Authorization extends Object {
	constructor() {
		super();

		this.checksumTable = uint8ArrayFromHex(checksumTable);
		this.aesIV = uint8ArrayFromHex(aesIV);
	}

	code(platform, serial, requestCode) {
		const plat = this.parsePlatform(platform);
		const sn = serialClean(serial);
		const reqCode = codeClean(requestCode);

		const request = this.requestDecode(sn, reqCode);
		const response = this.responseCreate(plat, sn, request);
		const encoded = this.responseEncode(sn, response);

		return codeFormat(encoded, 4);
	}

	requestDecode(serial, requestCode) {
		const data = base64Decode(requestCode);

		const key = this.serialToKey(serial);
		const aes = new Aes128Cbc(key, this.aesIV);
		const decrypted = aes.decrypt(data);

		const decLastByte = decrypted[31];
		if (decLastByte > 25) {
			throw new Error('Decryption error');
		}
		if (decLastByte - 25 > 57) {
			throw new Error('Decryption error');
		}

		const fromLast = 32 - decLastByte;
		const checksum = this.checksumData(decrypted, 31 - decLastByte);
		const checksumE = decrypted[fromLast - 1];
		if (checksum !== checksumE) {
			throw new Error(
				`Invalid checksum: ${checksum} != ${checksumE}`
			);
		}

		const smallData = new Uint8Array(3);
		smallData[2] = decrypted[fromLast - 2];
		smallData[1] = decrypted[fromLast - 3];
		smallData[0] = decrypted[fromLast - 4];

		const base64ed = base64Encode(
			uint8ArraySlice(decrypted, 0, fromLast - 4)
		);

		const base64Mixed = this.base64Mixer(base64ed);

		const base64dThing = base64Decode(base64Mixed);

		return {
			decrypted,
			smallData,
			base64dThing
		};
	}

	responseCreate(platformCode, serial, request) {
		const adomd5 = new AdoMd5();
		adomd5.update(bufferFrom(request.base64dThing));
		adomd5.update(serial);
		adomd5.update(bufferFrom(request.smallData));
		const hash = adomd5.digest();

		const hashBase64 = base64Encode(hash);

		const r = new Uint8Array(32);
		for (let i = 0; i < hashBase64.length; i++) {
			r[i] = hashBase64.charCodeAt(i);
		}

		r[24] = platformCode;
		r[25] = this.checksumData(r, 25);
		r[26] = 6;
		r[27] = 6;
		r[28] = 6;
		r[29] = 6;
		r[30] = 6;
		r[31] = 6;
		return r;
	}

	responseEncode(serial, response) {
		const key = this.serialToKey(serial);
		const aes = new Aes128Cbc(key, this.aesIV);
		const encrypted = aes.encrypt(response);

		return base64Encode(encrypted);
	}

	base64Mixer(str) {
		const strL = str.length;
		let r = '';
		for (let k = 0, l = 1; k < strL; k++) {
			if (l > strL - 1) {
				l = 0;
			}
			let limit = strL - 2;
			let i;
			for (i = l; i <= limit; limit--) {
				if (limit == i++) {
					i = 0;
				}
			}
			l++;
			r += str.charAt(i);
		}
		return r;
	}

	checksumData(data, size) {
		const table = this.checksumTable;
		let r = 0;
		for (let i = 0; i < size; i++) {
			r = table[data[i] ^ r];
		}
		return r;
	}

	serialToKey(serial) {
		const o = 8;
		const l = serial.length - o;
		const r = new Uint8Array(l);
		for (let i = 0; i < l; i++) {
			r[i] = serial.charCodeAt(i + o);
		}
		return r;
	}

	parsePlatform(platform) {
		const str = platformClean(platform);
		if (/^win/i.test(str)) {
			return 28;
		}
		if (/^mac/i.test(str)) {
			return 26;
		}
		throw new Error(
			`Unknown platform: ${platform} (expected win or mac)`
		);
	}
}
