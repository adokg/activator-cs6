import {Authorization} from './authorization';

export class Gui extends Object {
	constructor(
		form,
		platformName,
		serialNumberName,
		requestCodeName,
		responseCodeName
	) {
		super();

		this.form = form;
		this.platformName = platformName;
		this.serialNumberName = serialNumberName;
		this.requestCodeName = requestCodeName;
		this.responseCodeName = responseCodeName;
		this.submitCallback = null;
	}

	getFormElements(name) {
		return this.form.querySelectorAll(`[name="${name}"]`);
	}

	getFormValue(name) {
		const elements = this.getFormElements(name);
		for (const element of elements) {
			switch (element.type.toLowerCase()) {
				case 'checkbox': 
				case 'radio': {
					if (element.checked) {
						return element.value;
					}
					break;
				}
				default: {
					return element.value;
				}
			}
		}
		return null;
	}

	setFormValue(name, value) {
		const elements = this.getFormElements(name);
		for (const element of elements) {
			element.value = value;
		}
	}

	init() {
		this.submitCallback = e => {
			e.preventDefault();
			const platform = this.getFormValue(this.platformName);
			const serialNumber = this.getFormValue(this.serialNumberName);
			const requestCode = this.getFormValue(this.requestCodeName);

			const auth = new Authorization();
			try {
				const response = auth.code(platform, serialNumber, requestCode);
				this.setFormValue(this.responseCodeName, response);
			}
			catch (err) {
				this.setFormValue(this.responseCodeName, '' + err);
				throw err;
			}
		};

		this.form.addEventListener('submit', this.submitCallback);
	}

	destroy() {
		this.form.removeEventListener('submit', this.submitCallback);
	}
}
