import {Authorization} from './authorization';

export class Cli extends Object {
	constructor(log, error) {
		super();

		this.log = log;
		this.error = error;
	}

	async run(arg0, argv, env) {
		const args = argv.slice(2);
		if (args.length < 3) {
			this.log('args: platform serial request_code');
			return 1;
		}

		const platform = args[0];
		const serialNumber = args[1];
		const requestCode = args[2];

		const auth = new Authorization();
		const response = auth.code(platform, serialNumber, requestCode);

		this.log(response);

		return 0;
	}
}
