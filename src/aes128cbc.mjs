import crypto from 'crypto';

import {
	bufferFrom
} from './util';

export class Aes128Cbc extends Object {
	constructor(key, iv) {
		super();

		this.key = key;
		this.iv = iv;
	}

	encrypt(buff) {
		return this._process(
			crypto.createCipheriv('aes-128-cbc', this.key, this.iv),
			buff
		);
	}

	decrypt(buff) {
		return this._process(
			crypto.createDecipheriv('aes-128-cbc', this.key, this.iv),
			buff
		);
	}

	_process(crypt, buff) {
		crypt.setAutoPadding(false);
		const a = crypt.update(bufferFrom(buff));
		const b = crypt.final();
		return this._merge(a, b);
	}

	_merge(a, b) {
		const al = a.length;
		const l = al + b.length;
		const r = new Uint8Array(l);
		for (let i = 0; i < l; i++) {
			r[i] = i < al ? a[i] : b[i - al];
		}
		return r;
	}
}
