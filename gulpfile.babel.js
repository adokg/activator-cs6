import fs from 'fs';
import path from 'path';
import util from 'util';
import stream from 'stream';

import gulp from 'gulp';
import gulpSourcemaps from 'gulp-sourcemaps';
import gulpBabel from 'gulp-babel';
import gulpPug from 'gulp-pug';
import pug from 'pug';
import gulpPrettyHtml from 'gulp-pretty-html'
import browserslist from 'browserslist';
import supportsColors from 'supports-color';
import webpack from 'webpack';

const pipeline = util.promisify(stream.pipeline);
const readFile = util.promisify(fs.readFile);

const libDir = 'lib';
const guiDir = 'gui';

async function packageJson() {
	return JSON.parse(await readFile('package.json', 'utf8'));
}

async function babelrc() {
	const options = JSON.parse(await readFile('.babelrc', 'utf8'));
	options.babelrc = false;
	return options;
}

function babelrcGetEnv(opts) {
	for (const preset of opts.presets) {
		if (preset[0] === '@babel/preset-env') {
			return preset[1];
		}
	}
	return null;
}

async function browserslistrc() {
	const data = await readFile('.browserslistrc', 'utf8');
	return browserslist.parseConfig(data).defaults;
}

export async function lib() {
	const babelOptions = await babelrc();
	await pipeline(
		gulp.src([
			'src/**/*.{,m}js',
		]),
		gulpSourcemaps.init(),
		gulpBabel(babelOptions),
		gulpSourcemaps.write('.', {
			includeContent: true,
			destPath: libDir
		}),
		gulp.dest(libDir)
	);
}

async function guiBootstrap() {
	await pipeline(
		gulp.src([
			'node_modules/bootstrap/dist/css/bootstrap.css*'
		]),
		gulp.dest(guiDir)
	);
}

async function guiHtml() {
	const pkg = await packageJson();
	const locals = {
		title: pkg.description,
		license: pkg.license
	};
	await pipeline(
		gulp.src([
			'ui/**/*.pug'
		]),
		gulpPug({
			pug,
			locals
		}),
		gulpPrettyHtml({
			indent_with_tabs: true,
			end_with_newline: true
		}),
		gulp.dest(guiDir)
	);
}

async function guiJs() {
	const browsers = await browserslistrc();
	const babelOptions = await babelrc();
	const babelOptionsEnv = babelrcGetEnv(babelOptions);
	babelOptionsEnv.modules = false;
	babelOptionsEnv.targets = {
		browsers
	};

	const options = {
		entry: './src/index',
		output: {
			filename: `activator.js`,
			path: path.resolve(guiDir),
			library: 'activator',
			libraryTarget: 'umd'
		},
		mode: 'development',
		devtool: 'source-map',
		module: {
			rules: [
				{
					test: /\.(js|mjs)$/,
					exclude: /(node_modules)/,
					use: {
						loader: 'babel-loader',
						options: babelOptions
					}
				}
			]
		}
	};
	await new Promise((resolve, reject) => {
		const compiler = webpack(options);
		compiler.run((err, stats) => {
			if (stats) {
				err = err || (stats.hasErrors() ? stats.compilation.errors[0] : null);
				console.log(stats.toString({colors: supportsColors}));
			}
			if (err) {
				reject(err);
				return;
			}
			resolve();
		});
	});
}

export async function gui() {
	await Promise.all([
		guiBootstrap(),
		guiHtml(),
		guiJs()
	]);
}
